// console.log('hello world')


/* 
	fetch() - pangkuha ng data 
	.then - nagbbreakdown ng response nung promise. required mag arrow function
	.json() - conversion from json to javascript
*/

// fetch is a way to retrieve data from an API. The '.then' function waits for the data to be fulfilled before it moves on to other code. 

let posts = fetch('https://jsonplaceholder.typicode.com/posts') //fetch function returns a promise that is consumable using the '.then' syntax as well.
		.then((response) => response.json())  //.then - to keep things syncronus
		.then((json) => console.log(json))

console.log(posts)


// ---------------------------------------------------------

/*
	async & await - para syncronous din parang .then
*/

// async and await is the javascript ES6 equivalent of the '.then' syntax. It allows us to write more streamlined/clean code while utilizing an API fetching functionality. It basically makes our code asynchronous.
async function fetchPosts(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	console.log(result)

	let json = await result.json()
	console.log(json)
}
fetchPosts()